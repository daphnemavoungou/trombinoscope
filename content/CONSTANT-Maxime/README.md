---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Maxime
nom: CONSTANT

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---

# Profil 
![](https://steamuserimages-a.akamaihd.net/ugc/5088536233466642530/35D9F1EF85BA81DFB7F091E2AFF156F9C703CDC6/?imw=512&&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=false)  
Nom : CONSTANT  
Prénom : Maxime 

étudiant en première année de BTS Services Informatiques aux Organisations (SIO). Passionné par l'informatique depuis mon plus jeune âge, je me suis lancé dans ce programme pour approfondir mes compétences et en acquérir de nouvelles.

## Contact

Pour me contacter : [maxime.fortune08@gmail.com](mailto:maxime.fortune08@gmail.com)

# Formations 

1. [**Baccalauréat STI2D option SIN**](https://livet.paysdelaloire.e-lyco.fr/option-sin/) *2018-2021*
2. [**UFR Psychologie Nantes**](https://psychologie.univ-nantes.fr/)  *2021-2022* 
3. [**BTS SIO Services Informatiques aux Organisations**](https://coliniere.paysdelaloire.e-lyco.fr/nos-formations/les-bts/bts-sio/) *2022-2024*

# Competences

| Competence  | Niveau ou type      |
|:--------------- |:---------------:|
|  Dev web |  Python/HTML(CSS)/PHP |
|  Framework | Django |
|  BDD | SQL/DBeaver |
|  IDE | SublimeText/Pycharm/VSCodium |
|  API |  vers Jira  |
|  Anglais | B1  |


# Experiences Professionnelles

Stage chez [Velco](https://velco.tech/fr/)  
À la fin de ma première année en BTS Services Informatiques aux Organisations (SIO), j'ai eu l'opportunité d'accomplir un stage enrichissant chez Velco. Mon principal projet au cours de ce stage a été le développement d'un programme en Python, utilisant le framework Django, pour interagir avec l'outil JIRA via des appels API REST et la manipulation de données JSON.

Ce programme avait pour finalité de créer un système de suivi de développement complet, incluant un planning de tâches et le suivi des Sprints. J'ai ainsi acquis des compétences avancées en programmation Python, en gestion d'API REST, et en création d'interfaces utilisateur HTML/CSS.
