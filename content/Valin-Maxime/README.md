---
prenom: Maxime
nom: Valin
---
# Profil

![](https://framagit.org/BySplash/trombinoscope/-/raw/master/public/media/Valin-Maxime.png)

Etudiant en BTS SIO dans le lycée La Colinière à Nantes. Je suis passionné d'informatique et réalise sur mon temps libre des programmes depuis plusieurs années.

## Contact

- Téléphone : 07 68 81 24 66
- E-mail : <maximevalinpro@gmail.com>
- Réseaux Professionnels : [LinkedIn](https://www.linkedin.com/in/maxime-valin-05a816292/)


# Formations

- [2023-????] **BTS Informatique et Cybersécurité (SIO) - Lycée La Colinière**, _Nantes_
- [2020-2023] **Baccalauréat Général - Lycée Caroline Aigle**, _Nort-sur-Erdre_
- [2017-2020] **Collège Agnes Varda**, _Ligné_

Suite aux formations que j'ai réalisé, j'envisage de réaliser une spécialisation en développement (SLAM / Solutions Logicielles et Applications Métier)


# Compétences

| Compétence | Caractéristiques |
|-------------------------- | -----------------------------|
| Développement | Python, Java, HTML |
| IDE | Visual Studio Code, Sublime Text, Eclipse, Intellij IDEA, PyCharm |
| Langues | Français (Natif), Anglais (B2) |


# Expériences

## Informatique

- [2021-2023] **Spécialité Numérique & Sciences Informatiques** dans le cadre du baccalauréat général. Cela m'a apporté de nombreuses connaissances et notions en programmation, systèmes et réseaux, savoir réaliser différent travaux en groupe, la gestion du temps et l'organisation.

## Autres

- [2022 & 2023] **Entretien et surveillance d'une piscine** dans le cadre d'un job d'été. Réaliser ce travail m'a contraint à développer mon organisation, ma gestion du temps, la prise de décisions et la communication d'équipe.
- [2019] **Stage d'observation** dans le cadre de la classe de troisième dans l'entreprise _Mercuria_. Cette expérience m'a permit de comprendre comment fonctionne fonctionnent l'organisation et la communication de groupe.