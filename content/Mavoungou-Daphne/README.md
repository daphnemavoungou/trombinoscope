---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Daphne
nom: Mavoungou

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---

# Profil :

![Photo](https://framagit.org/daphnemavoungou/trombinoscope/public/media/daphnemavoungou.jpg) 


Je m'appelle Daphne Mavoungou et je suis étudiante en BTS SIO. Après avoir obtenu mon baccalauréat en Systèmes Numériques, option Réseau Informatique et Systèmes Communicants, j'ai décidé de me spécialiser davantage dans le domaine du réseau. J'ai toujours été passionnée par l'informatique et j'aimerais en faire mon métier. Je suis motivée, curieuse et prête à m'investir pleinement dans mon parcours d'études pour développer mes compétences en réseau et atteindre mes objectifs professionnels.


## CONTACTES :


**Téléphone :**[06 67 54 19 15 ](tel:+3367541915)  
**E-mail :** [daphnemavoungou5@gmail.com](mailto:daphnemavoungou5@gmail.com)

# FORMATIONS :

 
**BTS SIO (Services Informatiques aux Organisations) 1ère année (Nantes, 2023-2024)**<br> 
_spécialité envisagée : SISR (Solutions d'Infrastructure, Systèmes et Réseaux)_

**BAC PRO Système Numérique (2020– 2023)**<br>
_Option RISC (Réseaux Informatiques et Systèmes Communicants)_

**Première générale (2019)**<br> 

**Seconde générale (2018-2019)**<br> 

# COMPÉTENCES

| Catégorie     | Description |
| ----------- | ----------- |
| Informatique      | Diagnostic  pannes sur un ordinateur,installation et mise en services du parc informatique
| Logiciels et outils  | Cisco packet tracer,VMware,solidworks,ISI Proteus,GLPI,virtualbox,Arduino      |
|Réseaux  | Configurer l'adresse IP et le domaine propre pour les bureaux,mise en place des sous réseaux
|Programmation     | Les connaisences basique sur la langage Python,HTML5,CSS,SQL
|Téléphonie   | VoIP
|Systèmes d'exploitation |Windows, Ubuntu, Xubuntu

# EXPÉRIENCES :

**01/2023 - 02/2023 Stage support au utilisateur**

_stage effectuer dans le cadre d'un Mobilité Erasmus+ en Irlande_

**11/2022 – 01/2023  Stage Technicienne de proximité**

_CMI France (siege social) 3 Av. André Malraux, 92300 Levallois-Perret_

**01/2022 - 04/2022 Techncienne informatique**

_Mairie D'Arcueil 10 Avenue Paul Doumer 94110 Arcueil_

**05/2021 - 07/2023 Assistante informatique**

_A l'EXELLENCE Villebon-sur-Yvette, France_
