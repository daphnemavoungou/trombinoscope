---
title: Profil de Kader Belem
prenom : Kader
nom : Belem
---

# Profil
![photo](https://imagizer.imageshack.com/v2/280x200q70/923/54AYRn.jpg)

Je m'appelle Kader Belem, j'ai 18 ans, et j'habite à Saint-Nazaire. Après avoir obtenu mon baccalauréat professionnel en Système Numérique avec une option en Audio-Visuel, Réseau et Équipement Domestique, et avoir été accepté au lycée La Colinière à Nantes, je suis actuellement étudiant en première année de BTS SIO (Services Informatiques aux Organisations).

## Contact
Téléphone : [07.07.07.07.07](tel:+33707070707)
Mail : [Kaderbelem428@gmail.com](mailto:Kaderbelem428@gmail.com)

# Formations
- 2020-2023, **Baccalauréat Professionnel Système Numérique option B (Audio-Visuel, Réseau et Équipement Domestique**, _Lycée François Arago à Nantes (44000)_
- 2023-2024, **1ère année de BTS SIO**, _Lycée La Colinière à Nantes (44000)_

# Compétences
| Compétences | Niveau |
|--|--|
| **Dépannage** | Électroménager, Audio-visuel, Informatique |
| **Systèmes d'Exploitation** | Windows, Ubuntu, Windows Server 2012 |
| **Logiciels de Traitement de Texte, Présentation, Tableau** | Word, Powerpoint, Excel, LibreOffice |
| **Langues** | Français, Anglais, Allemand |

# Expériences
- 2022-2023, **Stage chez Espaces Mobile**, _44 Quai de la Fosse, 44000 Nantes_  
  - Vente, maintenance et dépannage informatique.

- 2022-2023, **Stage chez Blue Lab**, _47 Le Paquebot, 44600 Saint-Nazaire_  
  - Impression 3D.
  - Création d'un réseau local pour l'entreprise.

- 2021-2022, **Stage chez Services 111**, _111 Boulevard Ernest Dalby, 44000 Nantes_  
  - Dépannage de petit électroménager / SAV.
  - Vente et dépannage électroménager, télévision, hifi, vidéo.
  - Livraison et installation de produits audio-visuel et électroménager.

- 2021-2022, **Stage chez Delia Technologies**, _11 Impasse Juton, 44000 Nantes_ 
  - Développement de sites Web.

- 2020-2021, **Stage chez Aristide Briand**, _10 Boulevard Pierre de Coubertin, 44606 Saint-Nazaire_
  - Maintenance et dépannage informatique.
  - Création d'un réseau local.

- 2020-2021, **Stage chez Espace Mobile**, _44 Quai de la Fosse, 44000 Nantes_
  - Vente, maintenance et dépannage informatique.

- 2019-2020, **Stage chez Espace Mobile**, _44 Quai de la Fosse, 44000 Nantes_ 
  - Vente, maintenance et dépannage informatique.
