#
# Script de déploiement
# Le script s'execute automatiquement par CI/CD
#

# fonctions du script
source api/function.sh

# on créer un fichier temporaire pour la page index du site
INDEX=$(mktemp /tmp/foo-XXXXX)

# on parcourt les éléments contenus dans content
for content in content/*; do

    # on initialise les variables de metadonnées
    unset prenom nom avatar_url public_avatar avatar_img style

    # on récupère le nom du dossier de l'élève
    student=$content
    student_dir=$(echo $student/ | cut -d '/' -f 2 )

    # on contrôle que le nom du dossier lu est conforme au spécification (Nom-Prénom)
    if [ -d "$content" ] && [[ $( [ $(echo "$student_dir" | grep -E "[A-Za-z]+-[A-Za-z]+") ] && echo true ) ]]; then

        echo "Traitement du dossier \`$student\`
        "
        echo "  📂 $student"

        # on recherche le fichier README.md dans le dossier
        content_filepath=$(find $student -iname "*.md" -print)

        # on contrôle que le fichier trouvé est bien un fichier
        if [[ -f $content_filepath ]] ; then

            echo "   ↳ 📄 $content_filepath"

            # on génère le chemin du fichier html cible
            make_url=$( echo $student_dir | tr '[:upper:]' '[:lower:]' | sed 's/áàâäçéèêëîïìôöóùúüñÂÀÄÇÉÈÊËÎÏÔÖÙÜÑ/aaaaceeeeiiiooouuunAAACEEEEIIOOUUN/g' | sed 's/ /-/g' )
            public_filepath="public/$make_url.html"

            # on lit les variables du fichier README
            eval $( parse_yaml $content_filepath | sed 's/é/e/g' )

            # on controle que les variable de metadonnées sont bien alimentées
            if [[ -n $prenom ]] && [[ -n $nom ]]; then 

                echo "      ↳ 🏷️  prenom = $prenom"
                echo "      ↳ 🏷️  nom    = $nom"

                # PHOTO PROFIL
                # on recherche l'image de profil dans le fichier README 
                avatar_alt=$( cat $content_filepath | grep -m 1 -oE '!.+\(.+\)' | grep -oE '\[.+\]' | tr '[]' '  ' | xargs )
                avatar_url=$( cat $content_filepath | grep -m 1 -oE '!.+\(.+\)' | grep -oE '\(.+\)' | tr '()' '  ' | xargs )

                if [[ -n $avatar_url ]]; then


                    echo "      ↳ 🖼️  avatar = $avatar_url"

                    # valeurs par défaut
                    public_avatar="media/default.jpg" 
                    avatar_alt="Image de profil par défaut représentant un personnage stylisé"

                    # on télécharge l'image si elle est hébergée et qu'elle possède une extension
                    ext_auth=("jpg" "JPG" "jpeg" "JPEG" "png" "PNG" "webp" "WEBP")
                    if [[ $avatar_url =~ ^http ]] || [[ $avatar_url =~ ^content/ ]] || [[ $avatar_url =~ ^media/ ]] ; then

                        avatar_url_ext=$(basename ${avatar_url##*.})

                        if [[ ${ext_auth[*]} =~ $avatar_url_ext ]]; then 

                            public_avatar=media/$nom-$prenom.$avatar_url_ext  

                            if [[ $avatar_url =~ ^http ]] && [[ ! -f "public/$public_avatar" ]]; then                  
                                wget $avatar_url -O public/$public_avatar
                            fi
                            
                            if [[ $avatar_url =~ ^content/ ]] && [[ ! -f "public/$public_avatar" ]]; then
                                cp $avatar_url public/$public_avatar
                            fi

                        fi
                    fi

                fi

                echo "                    $public_avatar"
                
                avatar_img="<img src=\"$public_avatar\" alt=\"$avatar_alt\" loading=\"lazy\">"


                

                # COURRIEL
                # on recherche le mail dans le fichier README
                courriel=$( cat $content_filepath | grep -m 1 -oE "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}" | xargs | cut -d ' ' -f 1 );
                if [[ -n $courriel ]]; then
                    courriel_link="<a href="mailto:$courriel" target="_blank" rel=\"noopener noreferrer\" title=\"Contacter par email (ouverture dans une nouvelle fenêtre)\" aria-label=\"Contacter par email\"><i class=\"icon-mail\" aria-hidden=\"true\"></i></a>"
                    echo "      ↳ 📧  email = $courriel"
                fi

                # on ajoute le nom et prénom de l'élève dans le fichier index
                echo "- <div>$avatar_img</div> <h3>$prenom $nom</h3><address class=\"promotion__eleve-liens\"><a href=\"./$make_url.html\" title=\"Accéder à la fiche de $prenom $nom\" aria-label=\"Voir le profil\"><i class=\"icon-profile\" aria-hidden=\"true\"></i></a>$courriel_link</address>" >> $INDEX

                # on controle s'il existe une feuille de style personnalisée
                #if [ -f "$student/asset/style.css" ]; then
                #    style=$(cat $student/asset/style.css)
                #fi

                # on convertit le fichier Markdown en HTML via pandoc
                pandoc \
                    -s \
                    -f markdown-auto_identifiers -t html5 \
                    --template template/student.html \
                    --metadata title="$prenom $nom" \
                    --shift-heading-level-by=1 \
                    -c assets/style.min.css \
                    $content_filepath \
                    -o $public_filepath

                echo ""
                echo "✅ Fichier HTML généré
                "
            else
                echo "Les variables \`prenom\` et \`nom\` ne sont pas trouvées dans le fichier README"
            fi
            

        else
            echo "❌ \`$content\` non comforme au format attendu
            "
        fi
    else
        echo "❌ \`$content\` n'est pas un dossier ou son nom n'est pas conforme au format attendu
        "
    fi

    echo "―――
    "
    
done

# On convertit le fichier temporaire du format Markdown au format HTML
pandoc \
        -s \
        -f markdown -t html5 \
        --template template/index.html \
        --metadata title="Index" \
        -c assets/style.min.css \
        $INDEX \
        -o "public/index.html"

# On supprime le fichier temporaire
rm $INDEX
    