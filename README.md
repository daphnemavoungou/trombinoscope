# Trombinoscope BTS SIO 🖼️

**Ce projet à pour objectif de mettre en place un trombinoscope des élèves du BTS SIO de la Colinière.**
Celui-ci a été mis en place dans le cadre des actions professionnelles de première année afin d'initier les élèves au format Markdown et aux langages du web.

Ce projet est accompagné d'un site web généré automatiquement par une routine d'intégration et de distribution continue (`CI/CD`)), hébergé par Framagit grace à `gitlab pages` et son code source est libre d'utilisation.

[Visiter le site](https://bts-sio-la-coliniere.frama.io/cephee/trombinoscope/)

## Créer son profil

- fork le dépôt
- Ajouter sa fiche dans le dossier `content/Nom-Prenom/README.md` en prenant comme exemple le fichier `content/_exemple/README.md` et en suivant la structure recommandée.
⚠️ Le nom du dossier sera utilisé pour créer l'url de la page. 
- Ajouter sa photo dans le dossier `public/media/Nom-Prenom.jpg`
⚠️ le chemin de l'image dans le fichier README.md doit être : `media/Nom-Prenom.jpg`.
- Faire une `pull request` avec pour titre `Nouveau profil : Prénom Nom` ou `Modification profil : Prénom Nom`
- La requète de fusion sera analysé par un⋅e admin avant d'être validé ou commentée
- Votre profil est ajouté au site 🎉⋅

La partie Markdown du fichier respecter la structure suivante : 
- Profil (titre de niveau 1)
    - Photo de profil (250x250)
    - Courte description
    - Contact (titre de niveau 2)(attention, ces données seront publiques)
        - courriel  
        - téléphone
- Formations (titre de niveau 1)
    - liste des formations
- Compétences (titre de niveau 1)
    - Tableau des compétences (colonne 1: Compétence, colonne 2: Compétences séparées par une virgule)
- Expériences (titre de niveau 1)
    - Informatiques (titre de niveau 2)
        - liste des expériences informatiques
    - Autres (titre de niveau 2)
        - liste des expériences autres
